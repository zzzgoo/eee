const path = require("path")
const HtmlWebpackPlugin = require('html-webpack-plugin')
const webpack = require("webpack")
const CopyWebpackPlugin = require('copy-webpack-plugin')
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin

const paras = {
	token: "TOKEN",
	oId: 100,
	adminId: "393496757384192",
	inputUserName: "Guest157734",
	qsType: 0,
	userType: 2,
	fr: 6,
}

module.exports = {
	entry: ["./src/main.js"],
	output: {
		path: path.resolve(__dirname, "../built/"),
		filename: 'bundle.[id].js',
		chunkFilename: "[id].js",
		publicPath: "/",
	},
	resolve: {
		alias: {
			"$root": path.resolve(__dirname, '../'),
			"$src": path.resolve(__dirname, '../src'),
			'assets': path.resolve(__dirname, '../src/assets'),
		},
	},
	module: {
		rules: [
			// {
			// 	test: /\.css$/,
			// 	use: [
			// 		process.env.NODE_ENV !== 'production' ? 'style-loader' : 'style-loader',
			// 		{
			// 			loader: 'css-loader',
			// 			options: {
			// 				importLoaders: 1,
			// 			},
			// 		},
			// 	],
			// },
			{
				test: /\.css|\.s(c|a)ss$/,
				use: [{
					loader: 'lit-scss-loader',
					options: {
						minify: true, // defaults to false
					},
				}, 'extract-loader', 'css-loader', 'sass-loader'],
			},
			{
				test: /\.(eot|svg|ttf|woff|woff2)(\?\S*)?$/,
				loader: 'file-loader',
				query: {
					name: '[name].[sha512:hash:base64:7].[ext]',
				},
			},
			{
				test: /\.ijpg(\?\S*)?$/,
				use: [
					{
						loader: 'url-loader',
						options: {
							limit: 1024 * 8,
						},
					},
				],
			},
			{
				test: /\.(png|jpe?g|gif|svg)(\?\S*)?$/,
				loader: 'file-loader',
				query: {
					name: '[name].[sha512:hash:base64:7].[ext]',
				},
			},
			{
				test: /\.(ogg|mp3)(\?\S*)?$/,
				loader: 'file-loader',
				query: {
					name: '[name].[sha512:hash:base64:7].[ext]',
				},
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			inject: true,
			// hash: true
			template: "./src/index.html",
			favicon: path.resolve(__dirname, `../asset/fav.ico`),
			minify: {removeComments: true, collapseWhitespace: true},
		}),
		new webpack.DefinePlugin({
			NODE_ENV: JSON.stringify(process.env.NODE_ENV),
			distributionName: JSON.stringify(process.env.distributionName),
			foroid: JSON.stringify(process.env.foroid),
			NPM_VERSION: JSON.stringify(process.env.npm_package_version),
			TIMEBILULD: JSON.stringify(new Date().valueOf()),
		}),
		new CopyWebpackPlugin([
			{from: path.resolve(__dirname, '../src/ie.html')},
		]),
		new CopyWebpackPlugin([
			{from: path.resolve(__dirname, '../static'), to: path.resolve(__dirname, '../built/static'), toType: "dir"},
		]),
		// new BundleAnalyzerPlugin({
		// 	analyzerPort: 8887,
		// }),
	],
	devServer: {
		host: "0.0.0.0",
		historyApiFallback: true,
		disableHostCheck: true,	// Invalid Host header
		port: 1234,
		public: `http://127.0.0.1:1234`,
		openPage: `./?fr=${paras.fr}&qstype=${paras.qsType}&userType=${paras.userType}&oId=${paras.oId}`,
	},
}
