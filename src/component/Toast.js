import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'

class Toast extends EnhancedElement {
	constructor(content) {
		super()
		this.content = content
	}
	render() {
		return html`
			<div class="toast">
				${this.content}
			</div>
		`
	}
	glassClick_cb() {
		if (this.closeOnClickModal) {
			this.hide()
		}
	}
	hide() {
		this.remove()
	}
	static show(config) {
		// customElements.whenDefined(button.localName)
		const instance = new Toast(config.content)
		document.body.append(instance)
		window.setTimeout(()=>{
			instance.hide()
		}, config.timeout || Toast.timeout)
		return instance
	}
	static setTimeout(num) {
		Toast.timeout = num || 30 * 1000
	}
	static get styles() {
		return css`
			:host{
				width: 100%;
				background-color: rgba(0,0,0,.6);
				color: white;
				text-align: center;
				padding: 15px 0;
				display: block;
				position: fixed;
				top: 0;
				left: 0;
				z-index: 9999;
			}
		`
	}
}

Toast.timeout = 10 * 1000
customElements.define('lit-toast', Toast)
export default Toast
