import {html, css} from 'lit-element'
// eslint-disable-next-line node/no-extraneous-import
import {unsafeHTML} from 'lit-html/directives/unsafe-html.js'
// eslint-disable-next-line node/no-extraneous-import
import {classMap} from 'lit-html/directives/class-map.js'
import EnhancedElement from './EnhancedElement'

class Record extends EnhancedElement {
	static get properties() {
		return {
			model: {type: Object},
			csName: {type: String},
		}
	}
	constructor() {
		super()
		this.model = {}
	}
	render() {
		const renderHeader = (model)=>{
			if (model.type === 100 || model.type === 101 || model.type === 777) {
				return html``
			}
			return html`
				<slot class="header ${this.determinePosition()}" name="header">
					<div class="nickname">${model.direction ? html`${this.csName}` : html`我`}</div>
					<div class="time">${ html`${model.time}`}</div>
				</slot>
			`
		}
		const bodyClasses = classMap({
			"left": this.model.direction === 1,
			"right": ![100, 777, 1].includes(this.model.direction),
			"first-welcome": this.model.type === 777,
			"date-divider": this.model.type === 100,
			"infinity-indicator": this.model.type === 101,
		})
		return html`
			${renderHeader(this.model)}
			<div class="body ${bodyClasses}">
				${ html`${unsafeHTML(this.model.msg)}`}
				<slot name="append"></slot>
			</div>
			<div class="footer">
				<slot name="footer"></slot>
			</div>
		`
	}
	determinePosition() {
		if (this.model.type === 100 || this.model.type === 777) {
			return "center"
		}
		if (this.model.direction === 1) {
			return "left"
		}
		return "right"
	}
	static get styles() {
		return css`
			:host {
				display: flex;
				flex-direction: column;
				flex-wrap: nowrap;
				color: #333;
			}
			.header {
				color: #999;
				display: flex;
				align-items: center;
			}
			.header .nickname {
				margin-left: .5rem;
			}
			.header .time {
				font-size: 70%;
				margin-left: 5px;
			}
			.body {
				max-width: 80%;
				margin: 5px;
				padding: 10px;
				border-radius: 15px;
			}
			.body.right.date-divider {
				background: #d6dfe8;
				display: inline-block;
				padding: 3px 10px;
				border-radius: 15px;
				font-size: 90%;
				color: #666;
				box-shadow: 0 1px 5px rgba(0,0,0,.2);
			}
			.body.right.infinity-indicator {
				margin: 0;
				padding: 0;
				background: none;
				box-shadow: none;
			}
			.body.left.first-welcome {
				max-width: none;
				background: none;
				color: #228605;
				font-weight: 700;
				text-align: left;
				box-shadow: none;
			}
			.body.left {
				max-width: 80%;
				background: #eaf7fd;
				margin: 5px;
				padding: 10px;
				word-break: break-all;
				border-radius: 15px;
				border-top-left-radius: 0;
				box-shadow: 0 1px 5px 0 rgba(0,0,0,.2);
			}
			.body.right {
				max-width: 80%;
				background: #f5f7fa;
				margin: 5px;
				padding: 10px;
				word-break: break-all;
				border-radius: 15px;
				border-top-right-radius: 0;
				box-shadow: 0 1px 5px 0 rgba(0,0,0,.2);
			}
			.body img {
				width: 40px;
				height: 40px;
			}
			.footer {
				width: 100%;
				color: #777;
				font-size: 90%;
				text-align: center;
			}
		`
	}
}

customElements.define('lit-record', Record)
