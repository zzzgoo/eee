import {html} from 'lit-element'
import EnhancedElement from './EnhancedElement'
// eslint-disable-next-line no-unused-vars
import {router, RouterSlot} from 'lit-element-router'
import routes from "$src/js/routes"
import "./Chat"
import "./Rate"
import "./End"

class App extends EnhancedElement {
	static get properties() {
		return {
			route: {type: String},
			params: {type: Object},
		}
	}
	constructor() {
		super()
		router(routes, (route, params, query) => {
			this.route = route
			this.params = params
		})
	}
	render() {
		return html`
            <router-slot route='${this.route}'>
				<lit-chat slot='chat'></lit-chat>
				<lit-rate slot='rate'></lit-rate>
				<lit-end slot='end'></lit-end>
				<div slot="nono">not found</div>
            </router-slot>
		`
	}
}

customElements.define('lit-app', App)
