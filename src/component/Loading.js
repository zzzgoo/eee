import {html, css, unsafeCSS} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import "./Mask"

class Loading extends EnhancedElement {
	static get properties() {
		return {
			message: {type: String},
		}
	}
	constructor() {
		super()
	}
	render() {
		return html`
			<lit-mask>
				<div slot="indicator" class="indicator"></div>
				<div slot="indicator" class="message">${this.message}</div>
			</lit-mask>
		`
	}
	static get styles() {
		return css`
			:host{
				display: block;
			}
			.indicator{
				background: url(${unsafeCSS(require("$root/asset/spinner.gif"))}) center/contain no-repeat;
				height: 70px;
				width: 70px;
			}
			.message{
				color: purple;
			}
		`
	}
}

customElements.define('lit-loading', Loading)

