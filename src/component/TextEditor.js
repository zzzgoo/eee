import {html} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import utils from "$src/js/utils"
import throttle from 'lodash.throttle'
import textEditorStyle from '$src/style/textEditor.scss'
import {disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock'

class TextEditor extends EnhancedElement {
	static get properties() {
		return {
			lineHeight: {type: Number, attribute: utils.camel2dash("lineHeight")},
			minRows: {type: Number, attribute: utils.camel2dash("minRows")},
			maxRows: {type: Number, attribute: utils.camel2dash("maxRows")},
			value: {type: String, reflect: true},
			disabled: {type: Boolean},
		}
	}
	constructor() {
		super()
		this._calrows = throttle(this.__calRows, 300, {"leading": true, 'trailing': true})
	}
	render() {
		return html`
			<textarea @input=${this.editorInput_cb} class="editor" ?disabled=${this.disabled}></textarea>
			<textarea class="copy" readonly rows="1"></textarea>
		`
	}
	firstUpdated() {
		this.editor_el = this.shadowRoot.querySelector(".editor")
		this.copy = this.shadowRoot.querySelector(".copy")
		this.editor_el.rows = this.minRows
		disableBodyScroll(this.editor_el)
	}
	editorInput_cb() {
		this.value = this.editor_el.value
		this._calrows()
		this.emit("input")
	}
	__calRows() {
		this.copy.rows = this.minRows
		this.copy.value = this.editor_el.value
		let rows = Math.floor(this.copy.scrollHeight / this.lineHeight)
		rows = Math.max(rows, this.minRows)
		rows = Math.min(rows, this.maxRows)
		if (rows === this.editor_el.rows) {
			return null
		}
		this.editor_el.rows = rows
	}
	watch() {
		return {
			value(value, old) {
				if (value == old) {
					return null
				}
				this.editor_el.value = value
				this.editor_el.dispatchEvent(new InputEvent("input"))
			},
		}
	}
	static get styles() {
		return [textEditorStyle]
	}
}

customElements.define('lit-text-editor', TextEditor)
