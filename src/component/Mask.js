import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import utils from "$src/js/utils.js"

class Mask extends EnhancedElement {
	static get properties() {
		return {
			closeOnClickModal: {type: Boolean, attribute: utils.camel2dash("closeOnClickModal")},
		}
	}
	constructor() {
		super()
		this.className = `lit-mask-${utils.random()}`
		// this.closeOnClickModal = true
	}
	render() {
		return html`
			<div class="mask">
				<div class="glass" @click=${this.glassClick_cb}>
					<slot @click=${this.contentClick_cb} name="indicator"></slot>
				</div>
				<slot name="content"></slot>
			</div>
		`
	}
	contentClick_cb(e) {
		// e.stopImmediatePropagation()
	}
	glassClick_cb() {
		if (this.closeOnClickModal) {
			this.hide()
		}
	}
	hide() {
		this.remove()
		this.emit('after-close', {})
	}
	static get styles() {
		return css`
			:host{
				display: block;
			}
			.glass{
				position: fixed;
				top: 0px;
				left: 0px;
				right: 0px;
				bottom: 0px;
				overflow: auto;
				background-color: rgba(0, 0, 0, .5);
				display: flex;
				flex-direction: column;
				justify-content: center;
				align-items: center;
			}
		`
	}
}

customElements.define('lit-mask', Mask)
