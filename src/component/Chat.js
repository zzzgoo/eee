import {html} from 'lit-element'
// eslint-disable-next-line node/no-extraneous-import
import {classMap} from 'lit-html/directives/class-map.js'
import Page from "./Page"
import "./Loading"
import "./Record"
import "./FilePicker"
import "./EnlargableImg"
import "./TextEditor"
import "./EmojiPicker"
import "./InfiniteLoading"
import "./CountdownButton"
import Toast from "./Toast"
import globalConfig from "$src/js/config.js"
import utils from "$src/js/utils.js"
// import parser from "$src/js/parser.js"
import socketio from "$src/js/socket.io.js"
import {findSinglePage, addCustomerByFor, fetchHistoricalRecords} from "$src/js/api.js"
import {scrollIntoView} from 'scroll-js'
import {Howl} from 'howler'
// eslint-disable-next-line no-unused-vars
import {RouterLink} from 'lit-element-router'
import easyMark from 'easy-mark'
import {disableBodyScroll, enableBodyScroll, clearAllBodyScrollLocks} from 'body-scroll-lock'
import chatStyle from '$src/style/chat.scss'

class Chat extends Page {
	static get properties() {
		return {
			isLoading: {type: Boolean},
		}
	}
	constructor() {
		super()
		this.metaInfo = {}
		this.essentialData = {}
		this.chatRecords_arr = []
		this.styleHeight = 0
		this.conversationInfo = {
			uId: null,
			msg: null,
			userName: null,
			profilePhotoUrl: null,
			from: null,
			staffName: null,
		}
		this.isLoading = true
		this.isEmojiVisible = false
		this.isAllSet = false
		this.isHistoryWanted = false
		this.pager = {
			"pageNo": 1,
			"pageSize": 50,
			"total": 0,
			"orderField": null,
			"orderSort": null,
			"sizes": [20, 30, 50, 100],
		}
		let prefix = globalConfig.getAPIDomain() + "/apis/file/readImage?basePath="
		if (globalConfig.env === "dev") {
			// prefix = "https://callcenter.badej8888.com/file/readImage?basePath="
			prefix = "https://callcenter.skeab6666.com/file/readImage?basePath="
		}
		this.imgPrefix = prefix
		this.init()
	}
	render() {
		const renderFooter = (record) => {
			if (record.type === 101) {
				if (this.isHistoryWanted) {
					return ""
				}
				return html`
					<button class="btn btn-gray" @click=${this.historyClick_cb} slot="footer">查看历史记录</button>
				`
			}
			if (record.type === 998) {
				if (record.isFrozen) {
					return html `
						<div class="appraise" slot="footer">${record.isResumed ? '您选择了继续对话' : '对话已关闭，如有其它问题，请返回再次联系客服'}</div>
					`
				}
				return html `
					<div class="appraise" slot="footer">若要继续对话，请点击“继续对话”按钮...</div>
					<div slot="footer">
						<div class="my-2">
							<lit-countdown-button from=30 @end=${this.cancelClick_cb.bind(this, record)} @click=${this.reconClick_cb.bind(this, record)}></lit-countdown-button>
							<button class="btn btn-cancel" @click=${this.cancelClick_cb.bind(this, record)}>取消</button>
						</div>
					</div>
				`
			}
		}
		const renderAppend = (record)=>{
			if (record.type === 999) {
				return html`<div class="appraise fright link" @click=${this.rateClick_cb} slot="append">评价本次会话</div>`
			}
			return null
		}
		const renderRecord = (record) => {
			return html `
				<lit-record .model=${record} csname=${this.conversationInfo.staffName } class="${this.determinePosition(record)} ${record.type === 777 ? 'first-welcome' : ''}">
					${renderFooter(record)}
					${renderAppend(record)}
				</lit-record>
			`
		}
		const renderedRecords_arr = this.chatRecords_arr.map(record => renderRecord(record))
		return html`
			<header class="header" @click=${this.headClick_cb}>
				<div class="profile">
					<div class="avatar">
						<img src=${`static/avatar/${this.conversationInfo.profilePhotoUrl || '1'}.png`}>
					</div>
					<div class="username">${ this.conversationInfo.staffName }</div>
				</div>
			</header>

			<lit-infinite-loading class="main" ?disabled=${this.isAllSet && this.isHistoryWanted ? false : true} direction="toup" @click=${this.mainClick_cb} @finite=${this.finite_cb}>
				<div slot="state-loading">加载中...</div>
				<div slot="state-nomore">已无更多</div>
				<div class="conversation">
					${renderedRecords_arr}
				</div>
				<div class="bottom"></div>
			</lit-infinite-loading>

			<footer class=${classMap({"footer": true, "disabled": !this.isAllSet})} @click=${this.footerClick_cb}>
				<div class="editor-wrapper">
					<div class="editor-start">
						<lit-file-picker ?disabled=${!this.isAllSet} @pick=${this.filePicked_cb} .accept=${this.allowWxt} max-size=${this.maxSize || Number.MAX_SAFE_INTEGER}>
							<button>
								<span class="icon">
									<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24">
										<path fill="#409eff" d="M8.5,13.5L11,16.5L14.5,12L19,18H5M21,19V5C21,3.89 20.1,3 19,3H5A2,2 0 0,0 3,5V19A2,2 0 0,0 5,21H19A2,2 0 0,0 21,19Z" />
									</svg>
								</span>
							</button>
						</lit-file-picker>
					</div>

					<div class="editor-content">
						<div class="editor-inner">
							<lit-text-editor class="editor" @click=${this.inputFocus_cb} line-height="14" min-rows="1" max-rows="4" ?disabled=${!this.isAllSet}></lit-text-editor>
						</div>
					</div>

					<div class="editor-end">
						<button type="submit" class="submit" @click=${this.emojiClick_cb} ?disabled=${!this.isAllSet}>
							<span class="icon">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24">
									<path fill="#409eff" d="M12,2A10,10 0 0,0 2,12A10,10 0 0,0 12,22A10,10 0 0,0 22,12A10,10 0 0,0 12,2M12,20A8,8 0 0,1 4,12A8,8 0 0,1 12,4A8,8 0 0,1 20,12A8,8 0 0,1 12,20M10,9.5C10,10.3 9.3,11 8.5,11C7.7,11 7,10.3 7,9.5C7,8.7 7.7,8 8.5,8C9.3,8 10,8.7 10,9.5M12,17.23C10.25,17.23 8.71,16.5 7.81,15.42L9.23,14C9.68,14.72 10.75,15.23 12,15.23C13.25,15.23 14.32,14.72 14.77,14L16.19,15.42C15.29,16.5 13.75,17.23 12,17.23M17,10H13V9H17V10Z" />
								</svg>
							</span>
						</button>
						<button type="submit" class="submit" @click=${this.sendClick_cb} ?disabled=${!this.isAllSet}>
							<span class="icon">
								<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" viewBox="0 0 24 24">
									<path fill="#409eff" d="M2,21L23,12L2,3V10L17,12L2,14V21Z" />
								</svg>
							</span>
						</button>
					</div>
				</div>
				<div>
					${this.isEmojiVisible ? html`<lit-emoji-picker @pick=${this.emojiPicked_cb}></lit-emoji-picker>` : html``}
				</div>
			</footer>
			<div class="other">
				${this.isLoading ? html`<lit-loading message="正在为您连接客服..."><lit-loading>` : html``}
				<router-link class="go-rate" href='/rate'></router-link>
				${this.connectionWarning ? html`<div class="connection-warning">${this.connectionWarning}</div>` : html``}
			</div>
		`
	}
	firstUpdated() {
		this.editor_el = this.shadowRoot.querySelector(".editor")
		disableBodyScroll(this.shadowRoot.querySelector(".main"))
	}
	determinePosition(record) {
		if (record.type === 100) {
			return "center"
		}
		if (record.type === 101) {
			return "center"
		}
		if (record.direction === 1) {
			return "left"
		}
		return "right"
	}
	reconClick_cb(record) {
		record.isFrozen = true
		record.isResumed = true
		this.scrollToBottom()
		this.requestUpdate()
	}
	cancelClick_cb(record) {
		if (record.isFrozen) {
			return null
		}
		record.isFrozen = true
		record.isTimeout = true
		this.socket.close()
		this.scrollToBottom()
		this.chatRecords_arr.push({
			msgType: 0,
			type: 999,
			msg: "感谢您对【366彩票】的支持， 我们在线客服7*24小时全天候不间断的为您提供服务，谢谢!",
			direction: 1,
		})
		this.requestUpdate()
	}
	emojiPicked_cb(e) {
		this.editor_el.focus()
		this.isEmojiVisible = false
		this.sendMessage2({
			index: e.detail.id,
		})
	}
	sendClick_cb(e) {
		this.sendMessage({
			type: 0,
			msg: this.editor_el.value,
			msgType: 0,
		})
	}
	initMeta() {
		const url = new URL(window.location.href)
		this.metaInfo.fr = url.searchParams.get("fr")
		this.metaInfo.oId = url.searchParams.get("oId")
		this.metaInfo.uid = url.searchParams.get("uid")
		this.metaInfo.userName = url.searchParams.get("userName")
		this.metaInfo.token = url.searchParams.get("token") || url.searchParams.get("tk")
		this.metaInfo.isAndroid = url.searchParams.get("isAndroid") === 1
		this.metaInfo.deviceId = url.searchParams.get("deveiceId") || ""
		this.metaInfo.qstype = url.searchParams.get("qstype")
		this.metaInfo.status = url.searchParams.get("status") || 0
		this.metaInfo.channel_type = url.searchParams.get("channel_type") || 0
		this.metaInfo.isUser = !!this.metaInfo.token
		return Promise.resolve(undefined)
	}
	mainClick_cb() {
		this.blurEmoji()
	}
	headClick_cb() {
		this.blurEmoji()
	}
	footerClick_cb() {
		this.blurEmoji()
	}
	blurEmoji() {
		this.editor_el.blur()
		this.isEmojiVisible = false
		this.requestUpdate()
	}
	inputFocus_cb(e) {
		this.isEmojiVisible = false
		e.preventDefault()
		e.stopImmediatePropagation()
		// this.requestUpdate()
	}
	fetchEssentialData() {
		return findSinglePage({})
	}
	initSocket() {
		// 此處需要設置timeout
		return this.prepareURL().then((url) => {
			const queries = {}
			url.searchParams.forEach((item, key) => {
				queries[key] = item
			})
			return socketio.connect(url.origin, {
				path: '/socket.io',
				autoConnect: false,
				query: queries,
				reconnection: true,
				reconnectionDelay: 1 * 1000,
				reconnectionDelayMax: 10 * 1000,
				reconnectionAttempts: 999,
				transports: ["websocket"], // websocket polling
			})
		})
	}
	filterBadWords(str, bad) {
		if (!str) {
			return ""
		}
		if (!bad || !bad.length) {
			return str
		}
		bad = bad[0].word
		let regexp = bad.replace(/,/g, "|")
		regexp = new RegExp(`(${regexp})`, "g")
		return str.replace(regexp, "***")
	}
	finite_cb(e) {
		const feedback = e.detail.feedback
		this.pager.pageSize = 15
		fetchHistoricalRecords({untilId: this.lastId, uid: this.metaInfo.uid, customerId: this.metaInfo.customerId}, this.pager).then((data) => {
			if (!data.length) {
				return feedback.complete()
			}
			this.pager.pageNo++
			const records = data
			records.forEach((record) => {
				record.msg = record.content
				if (record.msgType == 1) {
					record.msg = `<lit-enlargable-img src=${this.imgPrefix + record.msg}> </lit-enlargable-img>`
				} else {
					// record.msg = utils.sanitize(record.msg)
					if (record.type === 3) {
						record.msg = this.emojiToImg(record.msg)
					} else {
						record.msg = this.filterBadWords(record.msg, this.essentialData.chatWordFilterList)
						record.msg = this.transferTags(record.msg)
						this.transferReply(record)
					}
				}
				if (record.type == 100) {
					delete record.direction
				}
				if (record.type == 100) {
					record.msg = utils.formatDay(record.time)
				}
				record.time = utils.formatTime(record.time)
				record.sendingState = 1
				this.chatRecords_arr.unshift(record)
			})
			// eslint-disable-next-line promise/no-nesting
			return this.requestUpdate().then(()=>{
				return feedback.success()
			})
		})
			.catch((e) => {
				Toast.show({content: e.message})
				feedback.fail()
			})
	}
	transferReply(record) {
		if (!record.reply) {
			return null
		}
		const result = `
				<div>
					<span style="color:#999;">&#10077;</span>
						${record.reply.replyContext}
					<span style="color:#999;">&#10078;</span>
				</div>
				<div class="context-reply small gray mt-2">${record.reply.replyClient} | ${record.reply.replyTime}</div>
				<div>${this.transferTags(record.msg)}</div>
			`
		record.msg = result
	}
	prepareURL() {
		const url = new URL(globalConfig.getSocketDomain())
		url.searchParams.set("fr", this.metaInfo.fr)
		url.searchParams.set("oId", this.metaInfo.oId)
		url.searchParams.set("qsType", this.metaInfo.qstype)
		url.searchParams.set("status", this.metaInfo.status)
		url.searchParams.set("channel_type", this.metaInfo.channel_type)
		url.searchParams.set("userType", this.metaInfo.uid ? 0 : 2)
		url.searchParams.set("token", this.metaInfo.token || "TOKEN")
		if (this.metaInfo.token) {
			return Promise.resolve(url)
		}
		return this.getAdminId().then((adminId) => {
			url.searchParams.set("adminId", adminId)
			return url
		})
	}
	getAdminId() {
		if (this.metaInfo.token) {
			return Promise.resolve(null)
		}
		if (this.metaInfo.customerId) {
			return Promise.resolve(this.metaInfo.customerId)
		}
		if (!this.metaInfo.isAndroid) {
			const adminId = localStorage.getItem("adminId")
			if (adminId) {
				this.metaInfo.customerId = adminId
				return Promise.resolve(adminId)
			}
		}
		return addCustomerByFor({
			deviceId: this.metaInfo.deveiceId,
		}).then((data) => {
			this.metaInfo.customerId = data
			if (!this.metaInfo.isAndroid) {
				localStorage.setItem("adminId", data)
			}
			return data
		})
	}
	filePicked_cb(e) {
		this.sendMessage({
			msg: e.detail,
			msgType: 1,
		})
	}
	configSocket(manager) {
		manager.on('connect', () => {
			console.info('[socket][connect]: connection established')
		})
			.on('disconnect', () => {
				this.isEmojiVisible = false
				// this.scrollToBottom()
				this.isAllSet = false
				this.requestUpdate()
				console.warn('[socket][disconnect]: connection corrupted')
			})
			.on("connect_timeout", ()=>{
				console.info('[socket][connect_timeout]')
			})
			.on('reconnect', (n) => {
				this.connectionWarning = "连接成功，请继续对话！"
				window.setTimeout(() => {
					this.connectionWarning = ""
				}, 2 * 1000)
				console.info('[socket][reconnect]: connection re-established')
			})
			.on('reconnect_attempt', () => {
				this.connectionWarning = "网络异常，正在尝试重新连接！"
				console.info('[socket][reconnect_attempt]: attempt to re-connect')
			})
			.on("lostAdmin", (body, fn) => {
				console.log("[socket][lostAdmin]")
				fn(1)
				this.socket.close()
				this.isAllSet = false
				Toast.show({
					content: "网络异常，正在重连...！",
				})
				window.setTimeout(() => {
					// eslint-disable-next-line promise/no-nesting
					this.initSocket().then((manager) => {
						return this.configSocket(manager)
					})
						.then(()=>{
							// this.isAllSet = true
							return Toast.show({
								content: "重连成功",
							})
						})
						.catch((e) => {
							return Toast.show({
								content: e.message,
							})
						})
				}, 2 * 1000)
			})
			.on('connect_error', (error) => {
				console.error('[socket][connect_error]:', error)
			})
			.on('reconnect_failed', (error) => { // 超过次数？
				console.error('[socket][reconnect_failed]:', error)
			})
			// eslint-disable-next-line promise/prefer-await-to-callbacks
			.on('close', (body, cb) => { // 主管断开连接
				this.chatRecords_arr.push({
					type: 999,
					msg: "会话结束，感谢您的反馈",
					direction: 1,
				})
				if (body) {
					// eslint-disable-next-line promise/no-callback-in-promise, promise/prefer-await-to-callbacks
					cb && cb(1)
				}
			})
			.on('error', (error) => {
				console.error('[socket][error]:', error)
				this.isAllSet = false
				error = error.replace(/，请留言/g, '，请稍后再联系')
				if (error.includes('null')) {
					Toast.show({
						content: "非法连接，请从正规网站连接进",
					})
				}
				Toast.show({
					content: error,
				})
			})
			.on('forceTrans', (body) => {
				this.conversationInfo.from = body.to
				console.info('[socket][forceTrans]:')
			})
			.on('transfer', (body) => {
				this.conversationInfo.from = body.to
				console.info('[socket][transfer]:')
			})
			.on('message', (body, fn) => {
				console.log("[socket][message]:", body)
				switch (body.type) {
				case 121:
					this.conversationInfo.staffName = body.msg
					break
				case 130:
					Object.assign(this.conversationInfo, body.msg)
					sessionStorage.setItem('chatId', body.msg.chatId) // 以备他用
					sessionStorage.setItem('oid', this.metaInfo.oId) // 以备他用
					break
				default:
					if (!this.lastId) {
						this.lastId = body.id
					}
					// eslint-disable-next-line no-case-declarations
					const record = {
						msgType: body.msgType,
						time: utils.formatTime(body.time),
						direction: 1,
						from: body.from,
						to: body.to,
						fromName: body.fromName,
						type: body.type,
						reply: body.reply,
					}
					record._original = body
					if (record.msgType == 1) {
						record.msg = `<lit-enlargable-img src=${body.msg}> </lit-enlargable-img>`
					} else {
						record.msg = body.msg
						// record.msg = utils.sanitize(record.msg)
						if (record.type === 3) {
							record.msg = this.emojiToImg(record.msg)
						} else {
							record.msg = this.filterBadWords(body.msg, this.essentialData.chatWordFilterList)
							record.msg = this.transferTags(record.msg)
							this.transferReply(record)
						}
						if (record.type == 998) {
							record.isFrozen = false
							record.isTimeout = false
							record.isResumed = false
						}
					}
					this.chatRecords_arr.push(record)
					this.requestUpdate()
					this.play()
					this.scrollToBottom()
					break
				}
			})
			.on('loginSelf', (body) => {
				console.info('[socket][loginSelf]:', body)
				if (body.fileAllow && body.fileAllow.AllowExt) {
					this.allowWxt = body.fileAllow.AllowExt
					this.maxSize = body.fileAllow.maxSize
				}
				this.conversationInfo.uId = body.uId
				this.conversationInfo.userName = body.userName
				this.conversationInfo.from = body.adminId,
				this.conversationInfo.staffName = body.friendlyName || body.adminName

				this.isLoading = false
				this.isAllSet = true
				if (!this.essentialData.welcomeMessageList || !this.essentialData.welcomeMessageList.length) {
					this.requestUpdate()
					return null
				}
				const newDate = new Date().valueOf()
				const list = [...this.essentialData.welcomeMessageList.filter(item => item.type == 5), ...this.essentialData.welcomeMessageList.filter(item => item.type == (this.metaInfo.isUser ? 1 : 2))]
				this.chatRecords_arr.push({type: 100, msg: utils.formatDay(new Date())})
				list.forEach((item, index) => {
					const defaultMsg =
						`
									欢迎光临！您已经进入服务队列，大概需要等待15秒。
									如果您对我们的产品或者服务有任何意见或者建议，
									都可以随时与我们在线取得联系！
									我们的客服人员会给您详尽细致的解答。
								`
					const record = {
						type: index == 0 ? 777 : 888,
						msgType: 0,
						direction: 1,
						msg: item.word,
						time: utils.formatTime(newDate),
					}
					if (index == 0 && !record.msg) {
						record.msg = defaultMsg
					}
					// record.msg = utils.sanitize(record.msg)
					record.msg = this.transferTags(record.msg)
					this.chatRecords_arr.push(record)
				})
				this.chatRecords_arr.unshift({
					type: 101,
					msgType: 0,
					msg: "",
					time: utils.formatTime(newDate),
				})
				this.requestUpdate()
				this.scrollToBottom()
			})
			.on('loginOther', (body, fn) => { // 建立连接之后，返回客服信息
				console.info('[socket][loginOther]:', body)
				fn(1)
			})
			.on('login', (o, fn) => {
				fn && fn(1)
			})
		this.socket = manager.open()
		return manager
	}
	emojiToImg(str) {
		if (!str) {
			return str
		}
		return str.replace(/\$::(\d+)::\$/, '<img src="/static/emoji/$1.gif">')
	}
	init() {
		this.initMeta().then(()=>{
			if (!this.metaInfo.oId || !this.metaInfo.fr || !this.metaInfo.qstype) {
				Toast.show({
					content: "非法连接，请从正规网站连接进入",
				})
				if (NODE_ENV != "dev") {
					window.location.href = "about:blank"
				}
			}
			return null
		})
			.then(() => Promise.all([this.fetchEssentialData(), this.initSocket()]))
			.then((results) => {
				const [essential, manager] = results
				Object.assign(this.essentialData, essential)
				return this.configSocket(manager)
			})
			.catch((e) => {
				Toast.show({
					content: e,
				})
			})
	}
	play() {
		new Howl({
			src: require("$root/asset/qqmsg.mp3"),
			autoplay: true,
		})
	}
	historyClick_cb() {
		this.isHistoryWanted = true
	}
	rateClick_cb() {
		this.socket.close()
		this.shadowRoot.querySelector(".go-rate").click()
	}
	transferTags(str) {
		if (!str) {
			return ""
		}
		return easyMark.render(str, {ownerName: this.ownerName, csName: this.conversationInfo.staffName, userName: this.conversationInfo.userName})
	}
	sendMessage2(emoji) {
		const entity = {
			from: this.conversationInfo.uId,
			fromName: this.conversationInfo.userName,
			msg: `$::${emoji.index}::$`,
			time: utils.formatTime(new Date()),
			to: this.conversationInfo.from,
			sendingState: 0,
			type: 3,
			direction: 0,
			msgType: 0,
		}
		entity.id = utils.random()
		if (!this.lastId) {
			this.lastId = entity.id
		}
		this.socket.emit('message', entity, (result) => {
			return entity.sendingState = result ? 1 : 2
		})
		entity.msg = `<img src="/static/emoji/${emoji.index}.gif">`
		this.chatRecords_arr.push(entity)
		this.editor_el.blur()
		this.scrollToBottom()
		this.requestUpdate()
	}
	emojiClick_cb(e) {
		this.isEmojiVisible = !this.isEmojiVisible
		e.stopImmediatePropagation()
		this.requestUpdate()
	}
	sendMessage(message) {
		if (!message.msg) {
			return null
		}
		if (!(message.msg || "").trim()) {
			return Toast.show({
				content: "您未填写如何内容",
			})
		}
		if (new Date() - this.newlySentTime < 1000) {
			return Toast.show({
				content: "消息发送太频繁，稍后重试",
			})
		}
		this.newlySentTime = new Date()
		Object.assign(message, {
			from: this.conversationInfo.uId,
			fromName: this.conversationInfo.userName,
			time: utils.formatTime(new Date()),
			sendingState: 0,
			direction: 0,
			to: this.conversationInfo.from,
		})
		message.id = utils.random()
		if (!this.lastId) {
			this.lastId = message.id
		}
		let aboutToSend = message.msg
		if (message.msgType === 1) {
			message.msg = `<lit-enlargable-img src=${message.msg}></lit-enlargable-img>`
			message.type = 1
		} else {
			// message.msg = utils.sanitize(message.msg)
			aboutToSend = message.msg
			message.msg = this.filterBadWords(message.msg, this.essentialData.chatWordFilterList)
			message.type = 0
			if (message.msg.includes('***')) {
				Toast.show({
					content: "请用文明字眼",
				})
			}
		}
		this.chatRecords_arr.push(message)
		this.editor_el.value = ""
		this.requestUpdate()
		this.socket.emit('message', Object.assign({}, message, {msg: aboutToSend.trim()}), (result) => {
			return message.sendingState = result ? 1 : 2
		})
		this.scrollToBottom()
	}
	scrollToBottom() {
		return scrollIntoView(this.shadowRoot.querySelector(".bottom"), this.shadowRoot.querySelector(".main"), {behavior: 'smooth'}).then(
			function() {
				return null
			}
		)
	}
	static get styles() {
		return [chatStyle]
	}
}

customElements.define('lit-chat', Chat)
