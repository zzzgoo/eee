import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import throttle from 'lodash.throttle'

const STATES = {
	IDLE: Symbol(), // 闲置状态
	LOADING: Symbol(), // 正在加载
	NOMORE: Symbol(), // 没有更多
	NORESULT: Symbol(), // 无数据
	ERROR: Symbol(), // 加载失败
}

class InfiniteLoading extends EnhancedElement {
	static get properties() {
		return {
			direction: {type: String},
			disabled: {type: Boolean},
			state: {type: Symbol},
		}
	}
	constructor() {
		super()
		this.direction = this.direction || "todown"
		this.feedback = {}
		this.state = STATES.IDLE
		this._figureoutState = throttle(this.__figureoutState, 50, {"leading": true, 'trailing': true})
		this.i = 0
	}
	render() {
		const renderStatePrompt = (direction)=>{
			if (this.direction == "todown" && direction === 1) {
				return html``
			}
			if (this.direction == "toup" && direction === -1) {
				return html``
			}
			if (this.state === STATES.NOMORE) {
				return html`<slot name="state-nomore">no more</slot>`
			}
			if (this.state === STATES.NORESULT) {
				return html`<slot name="state-noresult">no result</slot>`
			}
			if (this.state === STATES.ERROR) {
				return html`<slot name="state-error">error</slot>`
			}
			if (this.state === STATES.LOADING) {
				return html`<slot name="state-loading">loading</slot>`
			}
			if (this.state === STATES.IDLE) {
				return html`<slot name="state-idle"></slot>`
			}
		}
		return html`
			<div class="infinite-state-prompt">
				${renderStatePrompt(1)}
			</div>
			<slot class="content"></slot>
			<div class="infinite-state-prompt">
				${renderStatePrompt(-1)}
			</div>
		`
	}
	feedback_loaded() {
		// utils.nextTick(() => {
		// })
		if (this.isToup) {
			this.scrollTop = this.scrollHeight - this.oldHeight + this.oldTop
		}
		this.state = STATES.IDLE
	}
	feedback_completed() {
		this.state = STATES.NOMORE
	}
	firstUpdated() {
		this.isToup = this.direction === "toup"
		this.feedback.success = this.feedback_loaded.bind(this)
		this.feedback.complete = this.feedback_completed.bind(this)
		this.feedback.fail = this.feedback_errored.bind(this)
		this.addEventListener("scroll", () => {
			this._figureoutState()
		})
		this._figureoutState()
	}
	__figureoutState() {
		if (this.disabled) {
			return null
		}
		if (this.state !== STATES.IDLE) {
			return null
		}
		const distance = this.isToup ? this.scrollTop : this.scrollHeight - this.scrollTop - this.clientHeight
		if (distance > 30) {
			return null
		}
		// 距离偏移，影响计算
		this.state = STATES.LOADING
		if (this.isToup) {
			this.oldHeight = this.scrollHeight
			this.oldTop = this.scrollTop
		}
		this.finite()
	}
	feedback_errored() {
		// need a stratagy
		this.state = STATES.ERROR
	}
	finite() {
		// 需要防止死循環
		this.emit('finite', {
			feedback: this.feedback,
		})
	}
	watch() {
		return {
			disabled(value, old) {
				if (!this.disabled) {
					this._figureoutState()
				}
			},
		}
	}
	___connectedCallback() {
		super.connectedCallback()
		if (this) {
			return null
		}
		if (this.whereFrom()["isPc"]) {
			console.log(2222)
		} else {
			console.log('手机上运行')
			this.addEventListener('touchstart', (e) => {
				// console.log('初始位置：', e.touches[0].pageY)
				if (this.disabled) {
					return
				}
				if (this.state === STATES.NOMORE) {
					this.removeEventListener('touchstart', (event) => {
						event.preventDefault()
					}, false)
					return
				}
				this.startPos = e.touches[0].pageY
				this.style.position = 'relative'
				this.style.transition = 'transform 0s'
			}, false)
			this.addEventListener('touchmove', (e) => {
				if (this.disabled) {
					return
				}
				if (this.state === STATES.NOMORE) {
					this.removeEventListener('touchmove', (event) => {
						event.preventDefault()
					}, false)
					return
				}
				this.transitionHeight = e.touches[0].pageY - this.startPos
				if (this.transitionHeight > 0 && this.transitionHeight < 60) {
					this.style.transform = 'translateY(' + this.transitionHeight + 'px)'
					if (this.transitionHeight > 30) {
						this.state = STATES.LOADING
						if (this.indexNumber > 0) {
							return
						}
						this.indexNumber++
						setTimeout(() => {
							this.indexNumber = 0
						}, 500)
						return this.finite()
					}
				}
			}, false)
			this.addEventListener('touchend', (e) => {
				if (this.disabled) {
					return
				}
				this.style.transition = 'transform 0.2s ease 0.2s'
				this.style.transform = 'translateY(0px)'
				if (this.state === STATES.NOMORE) {
					this.removeEventListener('touchend', (event) => {
						event.preventDefault()
					}, false)
					return
				}
			}, false)
		}
	}
	static get styles() {
		return css`
			slot {
				color: #999;
				font-size: 90%;
			}
			.infinite-state-prompt{
				width: 100%;
				text-align: center;
			}
		`
	}
}

customElements.define('lit-infinite-loading', InfiniteLoading)

