import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'
class Star extends EnhancedElement {
	constructor() {
		super()
		this.isMouseover = false
		this.rank = 0
		this.rank_temp = 0
	}
	render() {
		const renderStar = ()=>{
			return [1, 2, 3, 4, 5].map((order)=>{
				return html`
					<span data-rank=${order} @mouseover=${this.mouseover_cb} @click=${this.click_cb}>
						<svg class=${`icon icon${order}`} t="1568012997298" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg" p-id="1953" width="200" height="200"><path d="M569.498552 94.591355l110.452601 223.834928 273.26778 40.687678c49.603745 7.397481 65.189721 42.312688 28.735461 80.583317l-199.268416 195.119933 48.854685 284.620339c6.563486 41.200354-33.247218 65.146743-68.225871 41.200354l-249.727645-130.273019L259.392772 963.953416c-34.038234 17.381879-69.038376 6.200212-62.688761-41.458227l47.764863-287.656489L39.10667 435.399391c-39.661302-36.582173-19.049868-69.979818 26.982538-76.094072l281.199427-40.85857c0 0 57.706283-117.0386 110.324688-223.64357C495.927953 23.221888 537.940812 24.269753 569.498552 94.591355z" p-id="1954"></path></svg>
					</span>
				`
			})
		}
		return html `
			<div class="title">
				服务评价
			</div>
			<div class=${`container rank${this.isMouseover ? this.rank_temp : this.rank}`} @mouseenter=${this.mouseenter_cb} @mouseleave=${this.mouseleave_cb}>
				${renderStar()}
			</div>
			<div class="tips">
				${['不满意', '一般', '基本满意', '满意', '非常满意'][this.rank]}
			</div>
		`
	}
	click_cb(e) {
		const target = e.currentTarget
		this.rank = target.dataset["rank"]
		this.requestUpdate()
		this.emit("pick", this.rank)
	}
	mouseleave_cb() {
		this.isMouseover = false
		this.requestUpdate()
	}
	mouseenter_cb() {
		this.isMouseover = true
	}
	mouseover_cb(e) {
		const target = e.currentTarget
		this.rank_temp = target.dataset["rank"]
		this.requestUpdate()
	}
	static get styles() {
		return css`
			:host {
				display: flex;
				align-items: center;
				justify-content: center;
				flex: 1;
				font-family: Arial;
				font-size: 14px;
				border-bottom: 1px solid #ddd;
			}
			.title, .tips {
				width: 100px;
				text-align: center;
				color: #666;
				font-weight: 700;
			}
			.container {
				display: flex;
				align-items: center;
			}
			.container span {
				width: auto;
				height: auto;
				display: inline-block;
				margin: 15px 10px;
			}
			.container .icon {
				width: 25px;
				height: 25px;
				fill: rgb(234, 234, 234);
			}
			.container.rank1 .icon1{
				fill: rgb(105, 105, 105);
			}
			.container.rank2 .icon1{
				fill: rgb(105, 105, 105);
			}
			.container.rank2 .icon2{
				fill: rgb(105, 105, 105);
			}
			.container.rank3 .icon1{
				fill: rgb(247, 186, 42);
			}
			.container.rank3 .icon2{
				fill: rgb(247, 186, 42);
			}
			.container.rank3 .icon3{
				fill: rgb(247, 186, 42);
			}
			.container.rank4 .icon1{
				fill: rgb(255, 0, 0);
			}
			.container.rank4 .icon2{
				fill: rgb(255, 0, 0);
			}
			.container.rank4 .icon3{
				fill: rgb(255, 0, 0);
			}
			.container.rank4 .icon4{
				fill: rgb(255, 0, 0);
			}
			.container.rank5 .icon{
				fill: rgb(255, 0, 0);
			}
		`
	}
}

customElements.define('lit-star', Star)
