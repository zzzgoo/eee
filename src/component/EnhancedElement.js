import {LitElement} from 'lit-element'

class EnhancedElement extends LitElement {
	attributeChangedCallback(attr, oldValue, newValue) {
		super.attributeChangedCallback(attr, oldValue, newValue)
		if (!this.watch || !this.watch.apply) {
			return null
		}
		if (!this._watch) {
			this._watch = this.watch()
		}
		if (this._watch.hasOwnProperty(attr)) {
			const watcher = this._watch[attr]
			watcher.bind(this, newValue, oldValue)()
		}
	}
	emit(eventName, payload) {
		const event = new CustomEvent(eventName, {
			detail: payload,
		})
		this.dispatchEvent(event)
	}
}

export default EnhancedElement

