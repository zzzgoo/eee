import {html, css} from 'lit-element'
import Page from './Page'
import "./Star"
import {saveChatScore} from "$src/js/api.js"
import utils from "$src/js/utils.js"

class Rate extends Page {
	constructor() {
		super()
		this.rank = 1
		this.isWizardVisible = false
	}
	render() {
		return html`
			<div>
				<lit-star @pick=${this.pick_cb}></lit-star>
			</div>
			<div class="type">
				<input type="radio" name="foo" id="rad1" value="0" /><label for="rad1">建议</label>
				<input type="radio" name="foo" id="rad2" value="1" /><label for="rad2">投诉</label>
			</div>
			<div class="separator"></div>
			<div class="textarea">
				<div class="textarea-wrapper">
					<textarea rows="8" class="editor" placeholder="描述具体情况，有助于我们优化服务" @keydown=${this.inputKeyDown_cb}></textarea>
				</div>
				<div class="subTitle">
					您的评价能帮助其他用户
				</div>
			</div>
			<div class="footer">
				<div class="footer-wrapper">
					<button class="btn btn-red" @click=${this.commitClick_cb}>提交评价</button>
					<button class=${`btn btn-gray${utils.checkMobile() ? '' : ' hidden'}`} @click=${this.endClick_cb}>结束评价</button>
				</div>
			</div>
			<router-link class="go-end" href='/end'></router-link>
			<div class=${this.isWizardVisible ? "" : "hidden"}></div>
		`
	}
	getType() {
		const radio = this.shadowRoot.querySelector(".type input:checked")
		if (radio) {
			return radio.vlaue
		}
		return null
	}
	pick_cb(e) {
		this.rank = e.detail
	}
	inputKeyDown_cb(e) {
		if (e.keyCode == 13) {
			e.preventDefault()
		}
	}
	endClick_cb() {
		this.isWizardVisible = true
	}
	commitClick_cb(e) {
		saveChatScore({
			"scoreType": this.getType(),
			"chatComment": this.shadowRoot.querySelector('.editor').value,
			"chatScore": +(this.rank || 5),
			"id": sessionStorage.getItem('chatId'),
			"oid": sessionStorage.getItem('oid'),
		}).catch((e)=>{
			//
		})
		this.shadowRoot.querySelector(".go-end").click()
	}
	static get styles() {
		return css`
			:host {
				display: block;
				margin: 0;
				padding: 0;
				box-shadow: 0 1px 5px 5px hsla(0,0%,69%,.2);
			}
			.hidden{
				display: none;
			}
			.type {
				width: 100%;
				height: 40px;
				padding: 20px 0;
				display: flex;
				justify-content: space-evenly;
				align-items: center;
			}
			.type input {
				display: none;
			}
			.type input[type="radio"]+label {
				background: #ddd;
				color: #666;
				margin-right: 10px;
				padding: 5px 15px;
				border-radius: 25px;
			}
			.type input[type="radio"]:checked+label {
				background: #db2828;
				color: white;
				margin-right: 10px;
				padding: 5px 15px;
				border-radius: 25px;
			}
			.type input:focus {
				-webkit-tap-highlight-color: rgba(0,0,0,0);
			}
			.separator {
				width: 100%;
				height: 10px;
				background: #f3f3f3;
				border-top: 1px solid hsla(0,0%,69%,.2);
				border-bottom: 1px solid hsla(0,0%,69%,.2);
			}
			.textarea {
				width: 100%;
				border: 0;
				margin: 0;
			}
			.textarea .textarea-wrapper {
				padding: 5px;
			}
			.textarea .textarea-wrapper textarea {
				width: 100%;
				resize: none;
				font-size: 14px;
				color: #666;
				border: 0;
				margin: 0;
				padding: 0;
			}
			.textarea textarea:focus {
				outline: none;
			}
			.textarea .subTitle {
				color: #999;
				font-size: 70%;
				text-align: right;
				padding: 10px;
				border-top: 1px solid #ddd;
			}
			.footer {
				width: 100%;
				position: absolute;
				bottom: 0;
				display: flex;
				flex-direction: column;
			}
			.footer .footer-wrapper {
				margin: 10px;
			}
			.btn {
				width: 100%;
				margin: .5rem 0;
				padding: 7px 15px;
				border-radius: 5px;
				color: white;
				font-size: 120%;
			}
			.btn:focus {
				outline: none;
			}
			.btn-red {
				background: #db2828;
			}
			.btn-gray {
				background: #909399;
			}
			::-webkit-input-placeholder {
				color: #999;
			}
		`
	}
}

customElements.define('lit-rate', Rate)
