import {html, css} from 'lit-element'
import Page from './Page'
import utils from "$src/js/utils.js"

class End extends Page {
	constructor() {
		super()
		this.isMaskVisible = false
	}
	render() {
		return html`
			<div class="picture">
				√
			</div>
			<div class="parag1">
				您的评价已提交，感谢您的支持！
			</div>
			<div class=${`parag2 ${utils.checkMobile() ? '' : 'hidden'}`}>
				请点击左上角 【&lt;】 按钮返回
			</div>
			<div class="footer">
				<button class=${utils.checkMobile() ? '' : 'hidden'} @click=${this.btnClick_cb}></button>
			</div>
			<div class="mask">  visard</div>
		`
	}
	btnClick_cb() {
		this.isMaskVisible = true
		this.requestUpdate()
	}
	static get styles() {
		return css`
			.hidden{
				display: none;
			}
		`
	}
}

customElements.define('lit-end', End)
