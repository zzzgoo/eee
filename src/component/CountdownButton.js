import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'

class CountdownButton extends EnhancedElement {
	static get properties() {
		return {
			num: {type: Number},
			from: {type: Number},
		}
	}
	constructor() {
		super()
	}
	render() {
		return html`
			<button class="btn btn-primary"><slot>继续对话 </slot>(${this.num})</button>
		`
	}
	firstUpdated() {
		this.num = this.from
		const handle = window.setInterval(()=>{
			if (this.num === 0) {
				window.clearInterval(handle)
				return this.emit("end", this.num)
			}
			this.num--
			this.emit("count", this.num)
		}, 1000 * 1)
	}
	static get styles() {
		return css`
			:host {
				display: inline;
			}
			.btn {
				background: #fff;
				color: #606266;
				margin: 0;
				padding: 3px 10px;
				border: 1px solid #dcdfe6;
				border-radius: 4px;
				display: inline-block;
				font-weight: 500;
				font-size: 12px;
				line-height: 1;
				cursor: pointer;
				white-space: nowrap;
				outline: 0;
				transition: .1s;
				box-sizing: border-box;
				-webkit-appearance: none;
			}
			.btn.btn-primary {
				color: white;
				background-color: #409EFF;
				border-color: #409EFF;
				padding: 10px 20px;
			}
		`
	}
}

customElements.define('lit-countdown-button', CountdownButton)

