import {html} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import utils from "$src/js/utils"
import Toast from './Toast'
import FilePickerStyle from '$src/style/filePicker.scss'

class FilePicker extends EnhancedElement {
	static get properties() {
		return {
			disabled: {type: Boolean},
			maxSize: {type: Number, attribute: utils.camel2dash("maxSize")},
			accept: {type: String},

		}
	}
	constructor() {
		super()
	}
	firstUpdated() {
		this.input_el = this.shadowRoot.querySelector(".input")
	}
	render() {
		return html`
			<input type="file" class="input" @change=${this.inputChange_cb} ?disabled=${this.disabled}>
			<span @click=${this.triggerClick_cb}>
				<slot> </slot>
			</span>
		`
	}
	inputChange_cb(e) {
		if (!e.target.files.length) {
			return null
		}
		const file = e.target.files[0]
		if (this.accept) {
			const segments = file.name.split(".")
			const ext = segments[segments.length - 1].toLowerCase()
			if (!this.accept.includes(ext)) {
				Toast.show({
					content: `只能上传以下格式：${this.accept.join(",")}`,
				})
				return this.input_el.value = null
			}
		}
		return utils.compressImage(file).then((base64)=>{
			if (file.size > 1024 * 1024 * this.maxSize) {
				Toast.show({
					content: `文件大小不能大于${this.maxSize}K`,
				})
				return this.input_el.value = null
			}
			this.emit("pick", base64)
			this.input_el.value = null
			return null
		})
	}
	triggerClick_cb() {
		if (this.disabled) {
			return null
		}
		this.shadowRoot.querySelector(".input").click()
	}
	static get styles() {
		return [FilePickerStyle]
	}
}

customElements.define('lit-file-picker', FilePicker)
