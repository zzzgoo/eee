import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import "./Mask"

class EnlargableImg extends EnhancedElement {
	static get properties() {
		return {
			isEnlarged: {type: Boolean},
			src: {type: String},
		}
	}
	constructor() {
		super()
		this.isEnlarged = false
	}
	_renderTemplate() {
		return html`
			<template class="templ">
				<lit-mask close-on-click-modal @after-close=${this.maskAfterClose_cb}>
					<img class="original loading" slot="indicator" src=${this.src} @load=${this.imgLoad_cb} />
					<style>
						.original{
							animation: zoomEffect .3s cubic-bezier(0.38, 0.1, 0.36, 0.9) forwards;
						}
						@keyframes zoomEffect {
							from {
								transform: scale(0);
								transition: all .5s ease-in-out;
							}

							to {
								transform: scale(1);
								transition: all .5s ease-in-out;
							}
						}
					</style>
				</lit-mask>
			</template>
		`
	}
	render() {
		return html`<img class="thumb loading" @load=${this.thumbLoad_cb} @click=${this.imgClick_cb} src=${this.src} />${this._renderTemplate()}`
	}
	imgClick_cb() {
		this.isEnlarged = true
		const templ = this.shadowRoot.querySelector(".templ")
		const clone = document.importNode(templ.content, true)
		clone.querySelector("lit-mask").addEventListener("after-close", this.maskAfterClose_cb.bind(this))
		const originalImg = clone.querySelector(".original")
		originalImg.addEventListener("load", this.imgLoad_cb.bind(this, originalImg))
		document.body.append(clone)
	}
	maskAfterClose_cb() {
		this.isEnlarged = false
	}
	thumbLoad_cb() {
		this.shadowRoot.querySelector(".thumb").classList.remove("loading")
	}
	imgLoad_cb(img, e) {
		img.classList.remove("loading")
	}
	static get styles() {
		const width = window.innerWidth
		const height = window.innerHeight
		return css`
			:host{
				display: block;
			}
			.thumb{
				max-width: 40px;
				max-height: 40px;
			}
			.original{
				max-width: ${width}px;
				max-height: ${height}px;
				border: 1px solid #ddd;
			}
			@keyframes zoomEffect {
				from {
					transform: scale(0);
					transition: all .5s ease-in-out;
				}

				to {
					transform: scale(1);
					transition: all .5s ease-in-out;
				}
			}
		`
	}
}

customElements.define('lit-enlargable-img', EnlargableImg)

