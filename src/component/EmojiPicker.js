import {html, css} from 'lit-element'
import EnhancedElement from './EnhancedElement'
import {Swiper, Pagination} from 'swiper/dist/js/swiper.esm.js'

class EmojiPicker extends EnhancedElement {
	static get properties() {
		return {
		}
	}
	constructor() {
		super()
		this.emoji_arr = new Array(88)
	}
	render() {
		return html`
			<link rel="stylesheet" type="text/css" href="/static/css/swiper.min.css">
			<div class="swiper-container">
				<div class="swiper-wrapper" @click=${this.emojiClick_cb}>
					<div class="swiper-slide">
						${this.renderPage(1)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(2)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(3)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(4)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(5)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(6)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(7)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(8)}
					</div>
					<div class="swiper-slide">
						${this.renderPage(9)}
					</div>
				</div>
				<div class="swiper-pagination"></div>
			</div>
		`
	}
	renderPage(n) {
		n = (n - 1) * 10
		const result = []
		for (let i = n; i < n + 10; i++) {
			result.push(html`<div class="img-box"><img data-id=${i} src="/static/emoji/${i}.gif" /></div>`)
		}
		return result
	}
	emojiClick_cb(e) {
		e.stopPropagation()
		if (e.target.tagName !== "IMG") {
			return null
		}
		this.emit("pick", {
			id: e.target.dataset["id"],
		})
	}
	firstUpdated() {
		super.firstUpdated()
		Swiper.use([Pagination])
		new Swiper(this.shadowRoot.querySelector(".swiper-container"), {
			direction: 'horizontal',
			loop: true,
			pagination: {
				el: this.shadowRoot.querySelector(".swiper-pagination"),
			},
		})
		this.addEventListener("click", (e)=>{
			e.stopPropagation()
		})
	}
	static get styles() {
		return css`
			:host{
				display: block;
			}
			.swiper-container {
				height: 220px;
			}
			.swiper-slide {
				height: 170px;
				display: flex;
				flex-wrap: wrap;
				overflow: hidden;
			}
			.swiper-pagination {
				bottom: -10px;
			}
			.swiper-slide-prev {
				display: none;
			}
			.img-box {
				width: 20%;
				height: 50%;
				display: flex;
				justify-content: center;
			}
			img {
				max-width: 90%;
				max-height: 80%;
				object-fit: contain;
			}
		`
	}
}

customElements.define('lit-emoji-picker', EmojiPicker)
