import globalConfig from './config.js'
import utils from '$src/js/utils.js'
// const token = globalConfig.token

export const addCustomerByFor = (body, pager) => {
	return post(`${globalConfig.getAPIDomain()}/customer/addCustomerByFor`, body, pager)
}
export const saveChatScore = (body, pager) => {
	return post(`${globalConfig.getAPIDomain()}/chat/saveChatScore`, body, pager)
}
export const findSinglePage = (body, pager) => {
	return post(`${globalConfig.getAPIDomain()}/ownerconfig/findSinglePage`, body, pager)
}
export const fetchHistoricalRecords = (body, pager) => {
	return post(`${globalConfig.getAPIDomain()}/chat/findUserChatMsg`, body, pager)
}


const fetchTimeout = 1000 * 14
const xxx = (f, pathName)=>{
	return new Promise(function(resolve, reject, time) {
		const t = window.setTimeout(function name(params) {
			reject(new Error(`网络访问超时，${pathName}`))
			window.clearTimeout(t)
		}, time || fetchTimeout)
		return f.then(resolve, reject)
	})
}
const get = (url, data = {}) => {
	const pathName = url.replace(/.*\/apis\//, '')
	utils.fix(data)
	let paras = ""
	for (const key in data) {
		if (Object.prototype.hasOwnProperty.call(data, key)) {
			paras += key + "=" + data[key] + "&"
		}
	}
	url += paras ? "?" + paras.substring(0, paras.length - 1) : ""
	const f = fetch(url, {method: "GET", headers: {"token": globalConfig.getToken()}})
		.then((resp) => {
			if (resp.ok) {
				return resp
			}
			const err = new Error(`${resp.statusText}，${pathName}`)
			err.code = resp.status
			throw err
		})
		.then(resp => resp.json())
		.then((data) => {
			if (data.code != 200) {
				console.info("GET请求失败：" + data.msg)
				const error = new Error(data.msg)
				error.code = data.code
				error.type = 110
				throw error
			}
			return data.data
		})
		// eslint-disable-next-line promise/prefer-await-to-callbacks
		.catch(function(err) {
			if (!err.code && !err.type) {
				err.message = `${err.message}，${pathName}`
			}
			throw err
		})
	return xxx(f, pathName)
}
const post = (url, body, pager, timeout) => {
	const pathName = url.replace(/.*\/apis\//, '')
	console.log("post请求，带入参数：", body)
	if (pager) {
		body = Object.assign({}, body, {"pageNo": pager.pageNo, "pageSize": pager.pageSize})
		if (!body.orderSort) {
			body.orderSort = pager.orderSort
			body.orderField = pager.orderField
		}
	}
	utils.fix(body)
	if (!body.orderSort) {
		body.orderField = "createTime"
		body.orderSort = "desc"
	}
	const headers = {
		"Content-Type": "application/json",
	}
	if (globalConfig.env === "dev") {
		headers["HTTP-CUST-OID"] = 100
	}

	const f = fetch(url, {method: "POST", headers, body: JSON.stringify(body)})
		.then((resp) => {
			if (resp.ok) {
				return resp
			}
			const err = new Error(`${resp.statusText}，${pathName}`)
			err.code = resp.status
			throw err
		})
		.then(resp => resp.json())
		.then(function(data) {
			if (data.code != 200) {
				console.info("POST请求失败：" + data.msg)
				const error = new Error(data.msg)
				error.code = data.code
				error.type = 110
				throw error
			}
			return data.data
		})
		// eslint-disable-next-line promise/prefer-await-to-callbacks
		.catch(function(err) {
			if (!err.code && !err.type) {
				err.message = `${err.message}，${pathName}`
			}
			throw err
		})
	return xxx(f, pathName)
}
