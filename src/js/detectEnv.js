const detectEnv = ()=>{
	let polyfills = []
	if (!('attachShadow' in Element.prototype && 'getRootNode' in Element.prototype) || window.ShadyDOM && window.ShadyDOM.force) {
		polyfills.push('sd')
	}
	if (!window.customElements || window.customElements.forcePolyfill) {
		polyfills.push('ce')
	}
	const needsTemplate = (function() {
		const t = document.createElement('template')
		if (!('content' in t)) {
			return true
		}
		if (!(t.content.cloneNode() instanceof DocumentFragment)) {
			return true
		}
		const t2 = document.createElement('template')
		t2.content.append(document.createElement('div'))
		t.content.append(t2)
		const clone = t.cloneNode(true)
		return clone.content.childNodes.length === 0 || clone.content.firstChild.content.childNodes.length === 0
	})()
	if (!window.Promise || !Array.from || !window.URL || !window.Symbol || needsTemplate) {
		polyfills = ['sd-ce-pf']
	}
	return polyfills.length
}

export default detectEnv
