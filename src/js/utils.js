import {isSameDay, isYesterday, isSameWeek, format} from 'date-fns'
import cn from 'date-fns/locale/zh_cn'
const local = {locale: cn}
const promi = Promise.resolve()

const fix = (obj) =>{
	for (const key in obj) {
		if (obj[key] == null || obj[key] === Number.MAX_SAFE_INTEGER) {
			delete obj[key]
		} else {
			if (obj[key] === -999111) {
				obj[key] = " "
			} else if (obj[key].constructor.name == "Date") {
				obj[key] = obj[key].valueOf()
			} else if (Array.isArray(obj[key]) && obj[key].length == 0) {
				delete obj[key]
			} else if (typeof obj[key] === "string") {
				obj[key] = obj[key].trim()
			}
		}
	}
}
const storage = {
	get(key) {
		const temp = sessionStorage.getItem(key)
		if (!temp || temp === "") {
			return null
		}
		return JSON.parse(temp)
	},
	set(key, obj) {
		sessionStorage.setItem(key, JSON.stringify(obj))
	},
}
const ensure2digits = (value)=> {
	if (typeof value == "string") {
		return "0" + value
	}
	if (value < 10) {
		return "0" + value
	}
	return value
}
const appendHTML = (pater, html)=> {
	const child = document.createElement("DIV")
	pater.append(child)
	child.outerHTML = html
}
const formatDay = (d) => {
	const that = new Date(d)
	const now = new Date()
	if (isSameDay(that, now)) {
		return "今日"
	}
	if (isYesterday(that)) {
		return "昨日"
	}
	if (isSameWeek(that, now)) {
		return format(that, "dddd", local)
	}
	return format(that, "M月D日", local)
}
const formatTime = (d) => {
	const day = new Date(d)
	return format(day, "HH:mm:ss", local)
}
const random = ()=>{
	return (Math.random() + "").slice(2)
}
const sanitize = (str)=>{
	if (!str) {
		return ""
	}
	return str.replace(/</g, "&lt;").replace(/>/g, "&gt;")
}
const camel2dash = (str) =>{
	if (!str) {
		return ""
	}
	return str.replace(/([a-z0-9]|(?=[A-Z]))([A-Z])/g, '$1-$2').toLowerCase()
}
const readBase64 = (blob) =>{
	return new Promise((resolve, reject) => {
		const reader = new FileReader()
		reader.addEventListener('load', () => {
			resolve(reader.result)
		})
		reader.addEventListener('error', (e) => {
			reject(e)
		})
		reader.readAsDataURL(blob)
	})
}
const drawImage = (src) => {
	const img = new Image()
	return new Promise((resolve, reject) => {
		img.addEventListener('load', () => {
			resolve(img)
		})
		img.addEventListener('error', (e) => {
			reject(e)
		})
		img.src = src
	})
}
const nextTick = (func, that) => {
	window.setTimeout(that ? func.bind(that) : func, 0)
}
const nextTask = (func, that) => {
	return promi.then(() => (that ? func.bind(that) : func)())
}
const compressImage = (file, ratio)=>{
	console.log(`压缩前：${file.size}`)
	return readBase64(file).then((base64) => {
		return drawImage(base64)
	})
		.then((image)=>{
			const canvas = document.createElement("canvas")
			const ctx = canvas.getContext('2d')
			const originWidth = image.naturalWidth
			const originHeight = image.naturalHeight
			const maxSize = 800
			let targetWidth = originWidth
			let targetHeight = originHeight
			if (originWidth > maxSize || originHeight > maxSize) {
				if (originWidth / originHeight > maxSize / maxSize) {
					targetWidth = maxSize
					targetHeight = Math.round(maxSize * (originHeight / originWidth))
				} else {
					targetHeight = maxSize
					targetWidth = Math.round(maxSize * (originWidth / originHeight))
				}
			}
			canvas.width = targetWidth
			canvas.height = targetHeight
			ctx.drawImage(image, 0, 0, originWidth, originHeight, 0, 0, targetWidth, targetHeight)
			return canvas
		})
		.then((canvas)=>{
			const data = canvas.toDataURL("image/jpeg", ratio || .8)


			let data_base64 = data.split(',')[1]
			data_base64 = window.atob(data_base64)
			console.log(`压缩后大小：${data_base64.length}`)


			return data
		})
}
const HIDDEN_STYLE = `
  visibility:hidden !important;
  overflow:hidden !important;
  position:absolute !important;
  z-index:-1000 !important;
  top:0 !important;
  right:0 !important
`
const CONTEXT_STYLE = [
	'letter-spacing',
	'line-height',
	'padding-top',
	'padding-bottom',
	'font-family',
	'font-weight',
	'font-size',
	'text-rendering',
	'text-transform',
	'width',
	'text-indent',
	'padding-left',
	'padding-right',
	'border-width',
	'box-sizing',
]
const calculateNodeStyling = (targetElement)=> {
	const style = window.getComputedStyle(targetElement)
	const boxSizing = style.getPropertyValue('box-sizing')
	const paddingSize =
		parseFloat(style.getPropertyValue('padding-bottom')) +
		parseFloat(style.getPropertyValue('padding-top'))

	const borderSize =
		parseFloat(style.getPropertyValue('border-bottom-width')) +
		parseFloat(style.getPropertyValue('border-top-width'))

	const contextStyle = CONTEXT_STYLE
		.map(name => `${name}:${style.getPropertyValue(name)}`)
		.join(';')
	return {contextStyle, paddingSize, borderSize, boxSizing}
}
const calcRows = (targetElement, minRows = 1, maxRows, lineHeight)=> {
	const copy = document.createElement('textarea')
	document.body.append(copy)
	const {contextStyle} = calculateNodeStyling(targetElement)
	copy.setAttribute('style', `${contextStyle};${HIDDEN_STYLE}`)
	copy.value = targetElement.value || targetElement.placeholder || ''
	copy.rows = minRows
	let rows = Math.floor(copy.scrollHeight / lineHeight)
	rows = Math.max(rows, minRows)
	rows = Math.min(rows, maxRows)
	copy.remove()
	return rows
}
const checkMobile = function() {
	let check = false;
	(function(a) {
		if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino|android|ipad|playbook|silk/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true
	})(navigator.userAgent || navigator.vendor || window.opera)
	return check
}
export default {
	fix,
	storage,
	ensure2digits,
	formatDay,
	appendHTML,
	formatTime,
	random,
	camel2dash,
	compressImage,
	sanitize,
	nextTick,
	calcRows,
	checkMobile,
	nextTask,
}
