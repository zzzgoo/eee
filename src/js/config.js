/* eslint-disable no-undef */
let token = null
let theme = null
export default {
	env: NODE_ENV ? NODE_ENV : "dev",
	getAPIDomain() {
		if (this.env === "dev") {
			// return "http://172.28.240.11:9999"		// 預生產http://callcenter.badej8888.com 
			return "http://172.28.200.34:9999"			// 測試http://callcenter.skeab6666.com/apis  
		}
		return "/apis"
	},
	getSocketDomain() {
		if (this.env === "dev" || window.location.href.includes("skeab6666")) {
			// return "http://livesupport.callcenter.badej8888.com"	// 預生產
			return "http://livesupport.callcenter.skeab6666.com:9190"// 測試
		}
		return window.location.origin
	},
	getToken() {
		if (!token) {
			token = sessionStorage.getItem("token")
		}
		return token
	},
	clear() {
		sessionStorage.removeItem('id')
		sessionStorage.removeItem('oid')
		sessionStorage.removeItem('user')
		sessionStorage.removeItem('token')
		token = null
		theme = null
		this.id = null
		localStorage.removeItem("theme")
	},
	getBuildTime() {
		return TIMEBILULD
	},
	getTheme() {
		if (theme == null) {
			theme = localStorage.getItem("theme")
		}
		return +theme
	},
	getOid() {
		//
	},
	constants: {
		version: 'v' + NPM_VERSION,
	},
}
